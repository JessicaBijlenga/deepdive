import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  readonly ROOT_URL = 'http://weerlive.nl/api/json-data-10min.php?key=d5e8569192&locatie=Utrecht'

  posts: any;

  constructor(private http: HttpClient) {}

  getPosts(){
    this.posts = this.http.get(this.ROOT_URL + '/posts')
  }

}
